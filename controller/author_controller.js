var author = require('../model/author')

exports.author_list = function (req, res) {


    author.find()
        .populate('authors')
        .exec(function (err, list_authors) {
            if (err) { return next(err); }
            //Successful, so render
            res.render('author_list', { title: 'Author List', author_list: list_authors });
        });
};