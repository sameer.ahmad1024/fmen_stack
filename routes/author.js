var express = require('express');

var router = express.Router();

var author_controller = require('../controller/author_controller')

router.get('/', author_controller.author_list);

module.exports = router;
