var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AuthorSchema = new Schema({
    first_name: { type: String, required: true, maxlength: 100 },
    last_name: { type: String, required: true, maxlength: 100 },
    family_name: { type: String, required: true, maxlength: 100 },
    date_of_birth: { type: Date },
    date_of_death: { type: Date },
});


AuthorSchema.virtual('name').get(function () {

    var full_name = "";
    if (this.first_name && this.last_name) {
        full_name = this.family_name + ', ' + this.first_name
    }
    if (!this.first_name || !this.family_name) {
        full_name = '';
    }

    return full_name;
});

module.exports = mongoose.model('authors', AuthorSchema)